# Revista_RIBES-Estilo_de_citas_y_referencias

Se trata de una adaptación del estilo de citas y referencias APA 7ª edición para RIBES (Revista de Investigación sobre Bibliotecas, Educación y Sociedad). Se ha realizado en base a la plantilla o estilo creado por Brenton M. Wiernik (zotero@wiernik.org) disponible en: https://www.zotero.org/styles/apa?source=1, que a su vez usa la plantilla de http://www.zotero.org/styles/apa-6th-edition

El archivo csl se puede añadir como estilo a los gestores de referencias, por ejemplo: Zotero, Mendeley, etc.

Por favor, si detectas algún problema o posible mejora, te agradecemos si nos lo comunicas a ribessecretaria@ucm.es.

Desde RIBES aconsejamos instalar este estilo. Pensamos que aligera en gran parte la tarea de dar formato a las citas y referencias de los manuscritos. No obstante, recordamos que en ocasiones son los metadatos de los trabajos citados y referenciados los que no están del todo bien redactados. Por ejemplo, al incluir algún documento desde algunas plataformas, el primer apellido se pone como segundo nombre y el segundo apellido queda como el único apellido. Por ello, recomendamos hacer una revisión de las citas y referencias aunque se use este estilo creado para RIBES.

El archivo csl se puede descargar pulsando con el botón derecho y guardando como, o usando la opción de descargar desde esta página: [estilo Revista-RIBES](https://codeberg.org/plr/Revista_RIBES-Estilo_de_citas_y_referencias/src/branch/main/revista-ribes.csl).

También, se puede descargar todo este proyecto en [formato zip](https://codeberg.org/plr/Revista_RIBES-Estilo_de_citas_y_referencias/archive/main.zip) e importar el archivo .cls al gestor de referencias.